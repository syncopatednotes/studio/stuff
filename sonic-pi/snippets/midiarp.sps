# key: midiarp
# point_line: 1
# point_index: 13
# --
# e.g. arpeggiate [4,2,3], :d3, :major, [0,1,2,3,4], 4, 80
midiarpeggiate [], tonic, mode, [], reps, velocity
